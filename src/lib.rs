use std::cell::Cell;
use std::cell::RefCell;
use std::rc::Rc;


type SharedMutableNode = Rc<RefCell<Node>>;
type SharedMutableValue = Rc<Cell<Option<f32>>>;
type SharedMutableVector = Rc<RefCell<Vec<SharedMutableNode>>>;


#[derive(Clone)]
enum BinaryOpKind {
    Add,
    Sub,
    Mul,
    Div,
    Pow,
}

#[derive(Clone)]
struct BinaryOp {
    left: SharedMutableNode,
    right: SharedMutableNode,
    kind: BinaryOpKind,
}

impl BinaryOp {
    fn perform_op(&self, lhs: f32, rhs: f32) -> f32 {
        match self.kind {
            BinaryOpKind::Add => lhs + rhs,
            BinaryOpKind::Sub => lhs - rhs,
            BinaryOpKind::Mul => lhs * rhs,
            BinaryOpKind::Div => lhs / rhs,
            BinaryOpKind::Pow => lhs.powf(rhs),
        }
    }
}

#[derive(Clone)]
enum UnaryOpKind {
    Sin,
    Cos,
}

#[derive(Clone)]
struct UnaryOp {
    operand: SharedMutableNode,
    kind: UnaryOpKind,
}

impl UnaryOp {
    fn perform_op(&self, operand: f32) -> f32 {
        match self.kind {
            UnaryOpKind::Sin => operand.sin(),
            UnaryOpKind::Cos => operand.cos(),
        }
    }
}

#[derive(Clone)]
pub struct Node {
    parents: SharedMutableVector,
    value: SharedMutableValue,
    kind: NodeKind,
}

#[derive(Clone)]
enum NodeKind {
    BinaryOp(BinaryOp),
    UnaryOp(UnaryOp),
    Input,
}

impl Node {
    pub fn compute(&self) -> f32 {
        if let Some(value) = (*self.value).get() {
            return value;
        }

        match &self.kind {
            NodeKind::BinaryOp(binary_node) => {
                let lhs = (*binary_node.left).borrow_mut().compute();
                let rhs = (*binary_node.right).borrow_mut().compute();
                (*self.value).set(Some(binary_node.perform_op(lhs, rhs)));
            },
            NodeKind::UnaryOp(unary_node) => {
                let operand = (*unary_node.operand).borrow_mut().compute();
                (*self.value).set(Some(unary_node.perform_op(operand)));
            },
            NodeKind::Input => (),
        }

        (self.value).get().unwrap()
    }

    pub fn set(&self, new_value: f32) {
        if let NodeKind::Input = self.kind {
            (*self.value).set(Some(new_value));
        }

        for parent in (*self.parents).borrow().iter() {
            (*parent).borrow_mut().invalidate_cache();
        }
    }

    fn invalidate_cache(&self) {
        if self.value.get().is_none() {
            return
        }

        self.value.set(None);
        for parent in (*self.parents).borrow().iter() {
            (*parent).borrow_mut().invalidate_cache();
        }
    }
}

impl From<f32> for Node {
    fn from(item: f32) -> Self {
        let new_node = create_input("literal");
        new_node.set(item);
        return new_node;
    }
}

pub fn create_input(_name: &str) -> Node {
    Node {
        parents: Rc::new(RefCell::new(vec![])),
        value: Rc::new(Cell::new(Some(f32::NAN))),
        kind: NodeKind::Input,
    }
}

fn create_binary_node(kind: BinaryOpKind, lhs: Node, rhs: Node) -> Node {
    let new_node = Node {
        parents: Rc::new(RefCell::new(vec![])),
        value: Rc::new(Cell::new(None)),
        kind: NodeKind::BinaryOp(
            BinaryOp {
                kind,
                left: Rc::new(RefCell::new(lhs.clone())),
                right: Rc::new(RefCell::new(rhs.clone())),
            }
        )
    };

    let node_ref = Rc::new(RefCell::new(new_node.clone()));
    (*lhs.parents).borrow_mut().push(Rc::clone(&node_ref));
    (*rhs.parents).borrow_mut().push(node_ref);

    new_node
}

fn create_unary_node(kind: UnaryOpKind, operand: Node) -> Node {
    let new_node = Node {
        parents: Rc::new(RefCell::new(vec![])),
        value: Rc::new(Cell::new(None)),
        kind: NodeKind::UnaryOp(
            UnaryOp {
                kind,
                operand: Rc::new(RefCell::new(operand.clone())),
            }
        ),
    };

    (*operand.parents).borrow_mut().push(Rc::new(RefCell::new(new_node.clone())));

    new_node
}

pub fn add<T: Into<Node>, V: Into<Node>>(lhs: T, rhs: V) -> Node {
    create_binary_node(BinaryOpKind::Add, lhs.into(), rhs.into())
}

pub fn sub<T: Into<Node>, V: Into<Node>>(lhs: T, rhs: V) -> Node {
    create_binary_node(BinaryOpKind::Sub, lhs.into(), rhs.into())
}

pub fn mul<T: Into<Node>, V: Into<Node>>(lhs: T, rhs: V) -> Node {
    create_binary_node(BinaryOpKind::Mul, lhs.into(), rhs.into())
}

pub fn div<T: Into<Node>, V: Into<Node>>(lhs: T, rhs: V) -> Node {
    create_binary_node(BinaryOpKind::Div, lhs.into(), rhs.into())
}

pub fn pow_f32<T: Into<Node>, V: Into<Node>>(lhs: T, rhs: V) -> Node {
    create_binary_node(BinaryOpKind::Pow, lhs.into(), rhs.into())
}

pub fn sin<T: Into<Node>>(operand: T) -> Node {
    create_unary_node(UnaryOpKind::Sin, operand.into())
}